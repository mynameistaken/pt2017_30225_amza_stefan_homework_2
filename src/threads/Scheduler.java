package threads;

import java.util.ArrayList;

public class Scheduler {
	private ArrayList<Server> servers = new ArrayList<Server>();
	private int maxServers;
	private int maxTasksPerServer;
	private Strategy strategy;
	
	public Scheduler(int maxserv, int maxtasks) {
		strategy = new QueueStrategy();
		maxServers = maxserv;
		maxTasksPerServer = maxtasks;
		for (int i = 0; i < maxServers; i++) {
			Server newServer = new Server(maxTasksPerServer);
			servers.add(newServer);
			Thread thread = new Thread(newServer);
			thread.start();
		}
	}
	
	public void changeStrategy (SelectionPolicy policy) {
		if (policy == SelectionPolicy.Shortest_Queue) 
			strategy = new QueueStrategy();
		if (policy == SelectionPolicy.Shortest_Time) 
			strategy = new TimeStrategy();
	}
	
	public void dispatch_Task(Task t) {
		strategy.addTask(servers, t);
	}
	
	public ArrayList<Server> getServers() {
		return servers;
	}
}
