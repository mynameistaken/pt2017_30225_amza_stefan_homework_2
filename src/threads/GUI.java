package threads;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.swing.*;

public class GUI extends JFrame{
	private static final long serialVersionUID= 1L;
	JPanel panel = new JPanel();
	private int width = 700, height = 800;
	
	Simulator sim;
	
	public GUI (Simulator s) {
		this.add(panel);
		sim=s;
		this.setSize(width,height);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		//panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
	}
	
	public void displayData(ArrayList<Server> servers) {
		panel.removeAll();
		sim.policy = SelectionPolicy.Shortest_Queue;
		sim.scheduler.changeStrategy(sim.policy);
		JPanel panel2 = new JPanel();
		
		this.validate();
		ArrayList<JScrollPane> scrolls = new ArrayList<JScrollPane>();
		ArrayList <JPanel> panels = new ArrayList <JPanel>();
		int i=0;
		for (Iterator<Server> it = servers.iterator(); it.hasNext();) {
			
			panels.add(i, new JPanel());
			panels.get(i).setLayout(new BoxLayout(panels.get(i), BoxLayout.Y_AXIS));
			panels.get(i).setBounds(i*200, 0, 200, 300);
			JLabel servername = new JLabel("Server "+(i+1));
			panels.get(i).add(servername);
			
			for (Task t : it.next().getTasks()) {
						JLabel aux = new JLabel(t.toString()); 
						panels.get(i).add(aux);		
			}
			
			JScrollPane aux2 = new JScrollPane(panels.get(i));
			scrolls.add(i, aux2);	
			i++;
		}
		
		for (JPanel p: panels) {
			panel.add(p);
			panel.add(Box.createRigidArea(new Dimension(100, 0)));
		}
		for (JScrollPane p: scrolls) {
			panel.add(p);
		}
		this.setVisible(true);
		this.repaint();
		this.revalidate();
	}

	public static void main(String[] args) {
		Simulator sim = new Simulator();
		Thread t = new Thread(sim);
		t.start();
		
	}

}
