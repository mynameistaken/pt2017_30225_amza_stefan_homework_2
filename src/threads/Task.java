package threads;

public class Task {

	private int processingTime;
	private int arrivalTime;
	
	public Task(int pt, int at) {
		processingTime = pt;
		arrivalTime = at;
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}
	
	@Override
	public String toString() {
		return "Task PT: " + processingTime + " AT: " + arrivalTime;
	}
	
}
