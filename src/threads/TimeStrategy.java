package threads;

import java.util.ArrayList;

public class TimeStrategy implements Strategy{

	@Override
	public void addTask(ArrayList <Server> servers, Task t) {
		int minTime = Integer.MAX_VALUE;
		int minIndex = 0;
		for (int i = 0; i < servers.size(); i++) {
			if (minTime > servers.get(i).getWaitingPeriod()) {
				minTime = servers.get(i).getWaitingPeriod();
				minIndex = i;
			}
		}
		servers.get(minIndex).addTask(t);
	}

}
