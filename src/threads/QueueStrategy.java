package threads;

import java.util.ArrayList;

public class QueueStrategy implements Strategy{

	@Override
	public void addTask(ArrayList <Server> servers, Task t) {
		int minLength = Integer.MAX_VALUE;
		int minIndex = 0;
		for (int i = 0; i < servers.size(); i++) {
			if (minLength > servers.get(i).getQueueLength()) {
				minLength = servers.get(i).getQueueLength();
				minIndex = i;
			}
		}
		servers.get(minIndex).addTask(t);
	}
}
