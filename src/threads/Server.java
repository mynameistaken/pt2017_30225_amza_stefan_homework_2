package threads;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable
{
	private ArrayBlockingQueue<Task> tasks;
	private AtomicInteger waitingPeriod;
	
	public Server(int maxTasks) 
	{
		waitingPeriod = new AtomicInteger(0);
		tasks = new ArrayBlockingQueue<Task>(maxTasks);
	}
	
	public void addTask(Task newTask) 
	{
		try {
			tasks.put(newTask);
		} 
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		waitingPeriod.addAndGet(newTask.getProcessingTime());
	}
	
	@Override
	public void run()
	{
		Task currentTask;
		while (true) {
			try {
				currentTask = tasks.peek();
				if (currentTask != null) {
				Thread.sleep(currentTask.getProcessingTime()*1000);
				waitingPeriod.set(waitingPeriod.get()-currentTask.getProcessingTime());
				tasks.remove();
				}
			} 
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public int getWaitingPeriod() 
	{
		return waitingPeriod.get();
	}
	public int getQueueLength() 
	{
		return tasks.size();
	}
	public ArrayBlockingQueue<Task> getTasks() 
	{
		return tasks;
	}
}
