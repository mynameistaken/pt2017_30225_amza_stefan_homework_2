package threads;

import java.util.ArrayList;

public interface Strategy {
	
	public void addTask(ArrayList <Server> servers, Task t);
		
}
