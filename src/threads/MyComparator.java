package threads;

import java.util.Comparator;

public class MyComparator implements Comparator<Task>{

	@Override
	public int compare(Task o1, Task o2) {
		return o1.getArrivalTime() > o2.getArrivalTime() ? 1 : o1.getArrivalTime() == o2.getArrivalTime() ? 0 : -1;
	}

}
