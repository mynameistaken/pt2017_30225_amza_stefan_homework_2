package threads;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class Simulator implements Runnable{

    Random randomGenerator = new Random();
	public int timeLimit = 300;
	public int maxProcessingTime = 20;
	public int minProcessingTime = 2;
	public int numberOfServers = 3;
	public int numberOfClients = 200;
	public SelectionPolicy policy = SelectionPolicy.Shortest_Time;
	public Scheduler scheduler;
	private GUI frame;
	private ArrayList<Task> generatedTasks;
	
	public Simulator() {
		scheduler = new Scheduler(numberOfServers, numberOfClients);
		scheduler.changeStrategy(policy);
		frame = new GUI(this);
		frame.displayData(scheduler.getServers());
		generatedTasks = new ArrayList<Task>();
		
	}
	
	public void generateTasks(int n) {
		while (n>0) {
			n--;
			Task t = new Task(Math.abs(randomGenerator.nextInt() % (maxProcessingTime-minProcessingTime)) + minProcessingTime, 
					Math.abs(randomGenerator.nextInt()%timeLimit));
			generatedTasks.add(t);
		}
		generatedTasks.sort(new MyComparator());
	}
	
	@Override
	public void run() {
		this.generateTasks(numberOfClients);
		int currentTime = 0;
		while (currentTime<timeLimit) {
			for (Iterator<Task> it = generatedTasks.iterator(); it.hasNext();){
				Task t = it.next();
				if (t.getArrivalTime() == currentTime) {
					scheduler.dispatch_Task(t);
					it.remove();
					
				}
			}try {
				Thread.currentThread().sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			currentTime++;
			System.out.println("CurrentTime: " + currentTime);
			frame.displayData(scheduler.getServers());
			try {
				Thread.currentThread().sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
